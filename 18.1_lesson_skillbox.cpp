#include <iostream>

class Stack
{

public:
    Stack(): size(0), StackArray (new int[size])
    {}

    //Add new element with resizing StackArray
    void push(int newElement)
    {
            //Create new Array with size + 1
            int* newArray = new int[size + 1];

            for (int i = 0; i < size; i++)
            {
                newArray[i] = StackArray[i];
            }
            newArray[size] = newElement;

            delete[] StackArray;
            StackArray = newArray;
            size++;
    }

    //Delete new element with resizing StackArray
    void pop()
    {
        if (size != 0)
        {
            //Create new Array with size - 1
            int* newArray = new int[size - 1];

            for (int i = 0; i < size - 1; i++)
            {
                newArray[i] = StackArray[i];
            }

            delete[] StackArray;
            StackArray = newArray;
            size--;
        }
    }

    //Return any element of Stack by element number
    int get(int index)
    {
        if ((index > 0) && (index <= size))
        {
            return StackArray[index - 1];
        }
        else
        {
            return 0;
        }
    }

    //Print all elements of Stack with std::cout
    void show()
    {
        for (int i = 0; i < size; i++)
        {
            std::cout << i << " " << StackArray[i] << "\n";
        }
        std::cout << "\n";
    }

private:
    int size;
    int* StackArray;

};

int main()
{
    //Test stack
    Stack st;
    std::cout << st.get(1) << "\n\n";

    st.push(12);
    st.push(15);
    st.push(-6);

    std::cout << st.get(2) << "\n\n";

    st.show();

    st.pop();
    st.show();

    st.push(47);
    st.show();

    st.push(351);
    st.show();

    st.pop();
    st.pop();
    st.pop();
    st.show();

    st.pop();
    st.show();
}
